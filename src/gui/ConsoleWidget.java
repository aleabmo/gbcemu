package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ConsoleWidget extends JPanel implements Console {
	/**
	 * Classe que permet imprimir per un jpanel el output de consola
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea output;
	
	public ConsoleWidget() {
		setLayout(new BorderLayout());
		output = new JTextArea();
		output.setCaretPosition(output.getDocument().getLength());
		JScrollPane scrollPane = new JScrollPane(output);
		scrollPane.setPreferredSize(new Dimension(500,100));
		add(scrollPane);

	}
	
	/**
	 * Funcio que implementa de la interficie, per tal de mostrar text per pantalla
	 */
	public void appendText(final String text) {
		if(EventQueue.isDispatchThread()) {
			output.setText(output.getText()+text);
		}else {

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					appendText(text);
				}
			});

		}
	}
}