package gui;

/**
 * Interface per tal de estandaritzar el tipus de consola
 * @author Aleix Abengochea
 *
 */
public interface Console {
	/**
	 * Per tal de poder fer mes d'un jpanel diferent, nomes cal implemnetar
	 * Consumer per tal de que StreamCapturer pugui ser generic
	 * @param text Text que s'imprimira per el output
	 */
	public void appendText(String text);
}
