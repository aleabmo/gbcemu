package gui;

import javax.swing.JButton;
import javax.swing.JToolBar;

public class ToolBar extends JToolBar{

	private static final long serialVersionUID = 1L;

	public JButton step = new JButton("Step");
	
	public ToolBar() {
		super();
		setFloatable(false);
		
		add(step);
	}

}
