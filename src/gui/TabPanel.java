package gui;

import javax.swing.JTabbedPane;

public class TabPanel extends JTabbedPane {
	private static final long serialVersionUID = 1L;

	private static final ConsoleWidget CAPTURE_PANE = new ConsoleWidget();

	public TabPanel() {
		super();
		add(CAPTURE_PANE, "Console");
	}
	
	public static ConsoleWidget getCapturePane() {
		return CAPTURE_PANE;
	}

}
