package utils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Tile {
    public static int BytesPerTile = 16;
    public static int Width = 8;
    public static int Height = 8;

    public int[] Pixels = new int[Width * Height];

    public int get(int x, int y) {
        return Pixels[x * Width + y];
    }

    public void set(int x, int y, int value) {
        Pixels[x * Width + y] = value;
    }

    public Tile(byte[] bytes) throws Exception {
        if (bytes.length != BytesPerTile)
            throw new Exception(" data must be BytesPerTile bytes.");

        for (int y = 0; y < Height; y++)
        {
            byte lowByte = bytes[2 * y];
            byte highByte = bytes[2 * y + 1];

            for (int x = 0; x < Width; x++)
            {
                int highBit = (highByte & (0b10000000 >> x)) > 0? 1 : 0;
                int lowBit = (lowByte & (0b10000000 >> x)) > 0 ? 1 : 0;
                set(x, y , 2 * highBit + lowBit);
            }
        }
    }

    public void DrawOnto(BufferedImage image, int tileX, int tileY, GameBoyColorPalette palette)
    {
        //BufferedImage img = new BufferedImage(128, 128, BufferedImage.TYPE_INT_RGB);
        var colors = palette.HexColors;
        for (int x = 0; x < Width; x++)
        {
            for (int y = 0; y < Height; y++)
            {
                var paletteIndex = get(x, y);
                var color = colors[paletteIndex];
                System.out.println(color);
                image.setRGB((tileX * Tile.Width) + x, (tileY * Tile.Height) + y, hex2Rgb(color).getRGB());
            }
        }
    }

    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
    }
}
