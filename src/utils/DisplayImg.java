package utils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DisplayImg extends JPanel {

    private static int outputImageWidthInTiles = 16;
    private static GameBoyColorPalette palette = GameBoyColorPalette.Dmg;

    BufferedImage img;

    public DisplayImg() throws Exception {

        File outputfile = new File("WEA.png");
        //Path path = Paths.get("Tetris.gb");
        //Path path = Paths.get("Tetrisdump.vram");
        Path path = Paths.get("POKEMON_RED.GB");
        System.out.println("Hello, World!");

        var rom = Files.readAllBytes(path);
        var tiles = Tiles.LoadFrom(rom);

        int outputImageWidthInPixels = outputImageWidthInTiles * Tile.Width;
        int outputImageHeightInPixels = GetOutputImageHeightInTiles(outputImageWidthInTiles, rom) * Tile.Height;
        img = new BufferedImage(outputImageWidthInPixels,outputImageHeightInPixels , BufferedImage.TYPE_INT_RGB);
        {
            for (int i = 0; i < Tiles.tiles.size(); i++)
            {
                int tileX = i % outputImageWidthInTiles;
                int tileY = i / outputImageWidthInTiles;
                tiles.get(i).DrawOnto(img, tileX, tileY, palette);
            }
            ImageIO.write(img, "png", outputfile);
        }
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, this); // see javadoc for more info on the parameters
    }

    private static int GetOutputImageHeightInTiles(int outputImageWidthInTiles, byte[] rom)
    {
        int tilesInRom = rom.length / Tile.BytesPerTile;
        return tilesInRom / outputImageWidthInTiles;
    }

}
