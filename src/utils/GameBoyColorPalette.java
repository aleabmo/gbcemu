package utils;

public class GameBoyColorPalette {

    //public IEnumerable<String> HexColors { get; set; }
    //public IEnumerable<Rgba32> ToRgba32() => HexColors.Select(c => Rgba32.ParseHex(c));
    public String[] HexColors = new String[4];

    private GameBoyColorPalette(String hexColors1, String hexColors2, String hexColors3, String hexColors4)
    {
        HexColors[0] = hexColors1;
        HexColors[1] = hexColors2;
        HexColors[2] = hexColors3;
        HexColors[3] = hexColors4;
    }

    /// <summary>
    /// The original Game Boy (aka DMG for "Dot Matrix Game") greenscale color palette.
    /// </summary>
    public static GameBoyColorPalette Dmg = new GameBoyColorPalette("#e0f8d0", "#88c070", "#346856", "#081820");

    /// <summary>
    /// The Game Boy Pocket grayscale color palette.
    /// </summary>
    public static GameBoyColorPalette Pocket = new GameBoyColorPalette("e8e8e8", "a0a0a0", "585858", "101010");

    //TODO: alternative color palettes the Game Boy Color sometimes used
    //https://en.wikipedia.org/wiki/Game_Boy_Color#Color_palettes_used_for_original_Game_Boy_games
}
