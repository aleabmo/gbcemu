package utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    private static int outputImageWidthInTiles = 16;
    private static GameBoyColorPalette palette = GameBoyColorPalette.Dmg;

    public static void main(String[] args) throws Exception {
        File outputfile = new File("BGMAP.png");
        //Path path = Paths.get("Tetris.gb");
        //Path path = Paths.get("Tetrisdump.vram");
        Path path = Paths.get("mapa.vram");
        System.out.println("Hello, World!");

        var rom = Files.readAllBytes(path);
        var tiles = Tiles.LoadFrom(rom);

        int outputImageWidthInPixels = outputImageWidthInTiles * Tile.Width;
        int outputImageHeightInPixels = GetOutputImageHeightInTiles(outputImageWidthInTiles, rom) * Tile.Height;
        BufferedImage img = new BufferedImage(outputImageWidthInPixels,outputImageHeightInPixels , BufferedImage.TYPE_INT_RGB);
        {
            for (int i = 0; i < Tiles.tiles.size(); i++)
            {
                int tileX = i % outputImageWidthInTiles;
                int tileY = i / outputImageWidthInTiles;
                tiles.get(i).DrawOnto(img, tileX, tileY, palette);
            }
            ImageIO.write(img, "png", outputfile);
        }

    }

    private static int GetOutputImageHeightInTiles(int outputImageWidthInTiles, byte[] rom)
    {
        int tilesInRom = rom.length / Tile.BytesPerTile;
        return tilesInRom / outputImageWidthInTiles;
    }
}
