package utils;

import java.util.ArrayList;
import java.util.List;

public class Tiles {
    /// Turn ROM data into a list of tiles by reading 16 bytes of tile data at a time.
    static List tiles = new ArrayList<Tile>();
    public static List<Tile> LoadFrom(byte[] rom) throws Exception {

        //var tileByte = Arrays.asList(rom);
        for (int i = 0; i < rom.length; i += Tile.BytesPerTile) {

            byte[] aux = new byte[Tile.BytesPerTile];
            for (int j = 0; j < Tile.BytesPerTile; j++) {
                aux[j] = rom[i + j];
            }
            tiles.add(new Tile(aux));
        }

        return tiles;
    }
}
