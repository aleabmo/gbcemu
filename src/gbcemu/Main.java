package gbcemu;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import gui.Display;
import gui.StreamCapturer;
import gui.TabPanel;
import gui.ToolBar;
import logic.Cpu;
import logic.Lr35902;
import logic.Lr35903full;
import logic.Ram;
import utils.DisplayImg;

public final class Main {
	
	
	Cpu cpu;
	
	static JPanel gui;
	static ToolBar tools;
	static TabPanel tabbedPane;
	static JFrame frame;
	static Display display;
	static DisplayImg prova;
	
	public Main() {
		initCpu();
		initGui();
	}
	
	public void initCpu() {
		Ram memoria = Ram.getInstance();
		
		//LD A, #6
		memoria.writeData( (short) 0x00, (byte) 0x01);
		memoria.writeData( (short) 0x01, (byte) 0x06);
		//LD C,A
		memoria.writeData( (short) 0x02, (byte) 0x03);
		//LD A, #7
		memoria.writeData( (short) 0x03, (byte) 0x01);
		memoria.writeData( (short) 0x04, (byte) 0x07);

		//ADD C, A
		memoria.writeData( (short) 0x05, (byte) 0x05);
		memoria.writeData( (short) 0x06, (byte) 0x01);

		//LD A, 0x22
		memoria.writeData( (short) 0x07, (byte) 0x22);
		//LD B, A
		memoria.writeData( (short) 0x08, (byte) 0x04);
		//LD [B], C
		memoria.writeData( (short) 0x09, (byte) 0x02);
		
		cpu = new Lr35903full();
	}
	
	public static void initGui() {
		gui = new JPanel(new BorderLayout());
		gui.setBorder(new EmptyBorder(0,0,0,0));
		
		tools = new ToolBar();
		tabbedPane = new TabPanel();
		display = new Display(2);
		display.setPreferredSize(new Dimension(160*2, 144*2));

		//Prova
		try {
			prova = new DisplayImg();
			prova.setPreferredSize(new Dimension(160, 144));
		} catch (Exception e) {
			e.printStackTrace();
		}

		gui.add(tools, BorderLayout.PAGE_START);
		gui.add(tabbedPane, BorderLayout.SOUTH);
		gui.add(display);

		//Prova
		gui.add(prova);

		frame = new JFrame("GBCEmu");
		frame.add(display);
		frame.add(gui);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationByPlatform(true);
		
		frame.pack();
		frame.setMinimumSize(frame.getSize());
		frame.setVisible(true);

		//new Thread(display).start();

	}
	
	public void step() {
		cpu.run();
		//display.enableLcd();
		//display.run();
		//display.requestRefresh();
		System.out.println(cpu);
	}

	public static void main(String[] args) {
		final Main app = new Main();
		
		tools.step.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				app.step();
			}
		});

		System.setOut(new PrintStream(new StreamCapturer(TabPanel.getCapturePane(), System.out)));

		System.out.println("Executant...");

		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				
			}
		});

	}
}
