package gbcemu;

import java.util.Scanner;

import logic.Cpu;
import logic.Lr35902;
import logic.Ram;

public class MainLogic {
	
	public static void main(String[] args) {

		/*
		Ram memoria = Ram.getInstance();
		memoria.writeData((short) 0x0F, (byte) 0xFF);
		byte result = memoria.getData((short) 0x0F);
		String s1 = String.format("%8s", Integer.toBinaryString(result & 0xFF)).replace(' ', '0');
		System.out.println(s1);
		*/

		Ram memoria = Ram.getInstance();
		
		//LD A, #6
		memoria.writeData( (short) 0x00, (byte) 0x01);
		memoria.writeData( (short) 0x01, (byte) 0x06);
		//LD C,A
		memoria.writeData( (short) 0x02, (byte) 0x03);
		//LD A, #7
		memoria.writeData( (short) 0x03, (byte) 0x01);
		memoria.writeData( (short) 0x04, (byte) 0x07);

		//ADD C, A
		memoria.writeData( (short) 0x05, (byte) 0x05);
		memoria.writeData( (short) 0x06, (byte) 0x01);

		//LD A, 0x22
		memoria.writeData( (short) 0x07, (byte) 0x22);
		//LD B, A
		memoria.writeData( (short) 0x08, (byte) 0x04);
		//LD [B], C
		memoria.writeData( (short) 0x09, (byte) 0x02);
		
		Cpu core = new Lr35902();
		
		int step = 0;
		Scanner sc= new Scanner(System.in);
		String exit ="";
		
		while(exit != "e") {
			step++;
			sc.nextLine();
			core.run();
			System.out.println(core);
			System.out.println(step);
			System.out.println("Memoria posicio 0x22: " + memoria.readData( (short) 0x22));
		}	
		sc.close();
	}
}
