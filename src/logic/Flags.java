package logic;

public class Flags {
    private static Flags instance;

    private boolean z;
    private boolean n;
    private boolean h;
    private boolean c;

    private Flags(){

    }

    public static Flags getInstance(){
        if(instance == null) instance = new Flags();
        return instance;
    }

    public void setZ(boolean val){
        this.z = val;
    }
    public boolean getZ(){
        return this.z;
    }

    public void setN(boolean val){
        this.n = val;
    }
    public boolean getN(){
        return this.n;
    }

    public void setH(boolean val){
        this.h = val;
    }
    public boolean getH(){
        return this.h;
    }

    public void setC(boolean val){
        this.c = val;
    }
    public boolean getC(){
        return this.c;
    }


}
