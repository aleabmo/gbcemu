package logic;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe que esta pensada per suscriure elements de mode que actua com si fos el bus
 * Els diferents components adre?ables s'inscriuran per tal de intercanviar dades entre ells
 * @author Aleix Abengochea
 *
 */
public class Mmu {

    //Array que contindra tots els objectes
    //private ArrayList<Addressable> map;
    private Addressable[] addrmap;

    public Mmu(int size){
        addrmap = new Addressable[size];
        //map = new ArrayList<Addressable>();
    }

    public void setDevice(Addressable dev, short start, short end, short cap){

    }

    public byte readAddr(short addr){
        addrmap[addr].readData(addr);
        return 0;
    }

}