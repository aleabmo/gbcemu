package logic;
public class Adder {
	private int input1 = 0, input2 = 0;
	private int output = 0;
	
	public Adder(int input1, int input2) {
		this.input1 = input1;
		this.input2 = input2;
	}
	
	public void operate() {
		this.output = this.input1 + this.input2;
	}
}