package logic;

public abstract class ChangeRegister {
	
	public static short ToShort(byte high, byte low) {
		return (short) ((high << 8) | low);
	}
	
	public static CompShort ToByte(short data) {
		byte high = (byte) (data >> 8);
		byte low = (byte) (data & 0xFF);
		return new CompShort(high, low);
	}
}
