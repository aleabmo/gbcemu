package logic;
/* Implementacio memoria ram 8kb.
*  Anire fent diferents proves de implementacio
*  De moment la implementare com un singleton, ja que no crec que sigui necessari tenir mes d'una
*  No la faig static per tal de no tenir problemes amb els methodes statics, i potser acabar derivant d'una interface.
*/
public class Ram implements Addressable {
	private static Ram instance;
	private byte[] memoria;
	
	private Ram() {
		memoria = new byte[8 * 1024];

		for (int i = 0; i< 8 * 1024; i++) {
			memoria[i] = 0x00;
		}
	}
	
	public static Ram getInstance() {
		if(instance == null) instance = new Ram();
		return instance;
	}
	
	public void writeData(short address, byte data) {
		memoria[address] = data;
	}
	
	public byte readData(short address) {
		return memoria[address];
	}
	
}
