package logic;

/**
 * Classe per tal d'implementar els registres del processador
 * Probablement hi haura que fer refactor per lidiar amb el 16bit mixtes
 * @author Aleix Abengochea
 *
 */
public class Registers {
	
	private static Registers instance;
	
	private byte A;
	private byte F;

	private byte B;
	private byte C;

	private byte D;
	private byte E;

	private byte H;
	private byte L;
	
	private short SP;
	private short PC;
	
	
	private Registers() {
		this.A = 0;
		this.F = 0;
		this.B = 0;
		this.C = 0;
		this.C = 0;
		this.D = 0;
		this.E = 0;
		this.H = 0;
		this.L = 0;

		this.SP = 0;
		this.PC = 0;
	}

	/**
	 * Fem que sigui un singelton
	 * @return
	 */
	public static Registers getInstance() {
		if(instance == null) instance = new Registers();
		return instance;
	}
	
	public void writeA(byte data) {
		this.A = data;
	}
	public byte readA() {
		return this.A;
	}
	
	public void writeB(byte data) {
		this.B = data;
	}
	public byte readB() {
		return this.B;
	}
	
	public void writeC(byte data) {
		this.C = data;
	}
	public byte readC() {
		return this.C;
	}
	
	public void writeF(byte data) {
		this.F = data;
	}
	public byte readF() {
		return this.F;
	}
	
	public void writeD(byte data) {
		this.D = data;
	}
	public byte readD() {
		return this.D;
	}
	
	public void writeE(byte data) {
		this.E = data;
	}
	public byte readE() {
		return this.E;
	}

	public void writeH(byte data) {
		this.H = data;
	}
	public byte readH() {
		return this.H;
	}

	public void writeL(byte data) {
		this.L = data;
	}
	public byte readL() {
		return this.L;
	}
	
	public void writeAF(short data) {
		CompShort aux = ChangeRegister.ToByte(data);
		this.A = aux.getHigh();
		this.F = aux.getLow();
	}
	public short readAF() {
		return ChangeRegister.ToShort(A, F);
	}

	public void writeBC(short data) {
		CompShort aux = ChangeRegister.ToByte(data);
		this.B = aux.getHigh();
		this.C = aux.getLow();
	}
	public short readBC() {
		return ChangeRegister.ToShort(B, C);
	}

	public void writeDE(short data) {
		CompShort aux = ChangeRegister.ToByte(data);
		this.D = aux.getHigh();
		this.E = aux.getLow();
	}
	public short readDE() {
		return ChangeRegister.ToShort(D, E);
	}

	public void writeHL(short data) {
		CompShort aux = ChangeRegister.ToByte(data);
		this.H = aux.getHigh();
		this.L = aux.getLow();
	}
	public short readHL() {
		return ChangeRegister.ToShort(H, L);
	}

	//16bit Registers

	public void writeSP(short data) {
		this.SP = data;
	}
	public short readSP() {
		return this.SP;
	}

	public void writePC(short data) {
		this.PC = data;
	}
	public short readPC() {
		return this.PC;
	}
}
