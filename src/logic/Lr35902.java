package logic;
/*
 * Intent d'implementació del sharp lr35902 ~ z80
 */
public class Lr35902 implements Cpu{
	
	private Ram memoria;
	
	private byte registreA;
	private byte registreB;
	private byte registreC;
	
	private byte flagZero;
	
	private byte registrePC;

	//Inicialitzem la memoria. Al principi facil, nomes un array i la inicialitzem fora
	public Lr35902() {
		this.memoria = Ram.getInstance();
		registreA = (byte) 0x00;
		registreB = (byte) 0x00;
		registreC = (byte) 0x00;
		
		registrePC = (byte) 0x00;
		
	}
	
	public void run() {
		
		//Fetch
		//Incrementem el program coutner per tal de anar a la seguent instruccio
		//!!! Tenir en compte depenen del opcode
		byte opcode = memoria.readData(registrePC++);
		
		//Decode
		switch(opcode) {
		
		//Execute
		
		/*
		 * LD A, #
		 * Exemple de com fer un load de un valor al registre A
		 * Utilitza dos cicles per tal de poder utilitzar 2bytes
		 */
		case 0x01:
			registreA = memoria.readData(registrePC++);
			break;

		/*
		 * LD [B], C
		 * Exemple de com fer un store del valor en el registre C
		 * a la direcció que conte el registre B
		 * Es un exemple, ja que haurien de ser 2bytes per en aquest exemple son de 8
		 */
		case 0x02:
			memoria.writeData(registreB, registreC);
			break;
			
		/*
		 * LD C, A
		 * Exemple de com fer un load de registre a registre
		 */
		case 0x03:
			registreC = registreA;
			break;
			
		/*
		* LD B, A
		* Exemple de com fer un load de registre a registre
		*/
		case 0x04:
			registreB = registreA;
			break;
			
		/*
		 * ADD A,C
		 * Exemple de com fem la suma de dos registres.
		 */
		case 0x05:
			registreC = (byte) (registreC + registreA);
			if(registreC == 0) flagZero = 0;
			break;
			
		/*
		 * JMP
		 * Exemple de jump
		 */
		case 0x06:
			registrePC = memoria.readData(registrePC++);
			break;
			
		/*
		 * NOP
		 * Implementem el nop a la 00 per tal de que no faci res 
		 * si troba un 00
		 */
		case 0x00:
			break;
			
		default:
			System.out.println("No existeix aquest opcode");
		}
		
	}
	
	/*
	 * override Tostring per tal de veure info del cpu
	 */
	public String toString() {
		return  "RegistreA: " + registreA + "\r\n" +
				"RegistreB: " + registreB + "\r\n" + 
				"RegistreC: " + registreC + "\r\n" +
				"flagZero: " + flagZero + "\r\n" +
				"RegistrePC: " + registrePC;
	}
}
