package logic;
/*
 * Intent d'implementaci? del sharp lr35902 ~ z80
 */
public class Lr35903opcodes implements Cpu{

	private Ram memoria;
	private Registers registres;
	private Flags flags;

	//Inicialitzem la memoria. Al principi facil, nomes un array i la inicialitzem fora
	public Lr35903opcodes() {
		this.memoria = Ram.getInstance();
		this.registres = Registers.getInstance();
		this.flags = Flags.getInstance();
	}
	
	public void run() {
		
		//Fetch
		//Incrementem el program coutner per tal de anar a la seguent instruccio
		//!!! Tenir en compte depenen del opcode
		short registrePC = registres.readPC();
		byte opcode = memoria.readData(registrePC++);
		registres.writePC(registrePC);
		
		//Decode
		switch(opcode) {

			case 0x00:
				break;

		/*
		 * LD A, #
		 * Exemple de com fer un load de un valor al registre A
		 * Utilitza dos cicles per tal de poder utilitzar 2bytes
		 */
		case 0x01:
			registrePC = registres.readPC();
			registres.writeA(memoria.readData(registrePC++));
			registres.writePC(registrePC);
			break;

		/*
		 * LD [B], C
		 * Exemple de com fer un store del valor en el registre C
		 * a la direcci? que conte el registre B
		 * Es un exemple, ja que haurien de ser 2bytes per en aquest exemple son de 8
		 */
		case 0x02:
			memoria.writeData(registres.readB(), registres.readC());
			break;
			
		/*
		 * LD C, A
		 * Exemple de com fer un load de registre a registre
		 */
		case 0x03:
			registres.writeC(registres.readA());
			break;
			
		/*
		* LD B, A
		* Exemple de com fer un load de registre a registre
		*/
		case 0x04:
			registres.writeB(registres.readA());
			break;
			
		/*
		 * ADD A,C
		 * Exemple de com fem la suma de dos registres.
		 */
		case 0x05:
			registres.writeC((byte) (registres.readC() + registres.readA()));
			if(registres.readC() == 0) flags.setZ(false);
			break;
			
		/*
		 * JMP
		 * Exemple de jump
		 */
		case 0x06:
			registrePC = registres.readPC();
			registres.writePC(memoria.readData(registrePC++));

			break;
			
		/*
		 * NOP
		 * Implementem el nop a la 00 per tal de que no faci res 
		 * si troba un 00
		 */
			case 0x07:
				break;

			case 0x08:
				break;

			case 0x09:
				break;

		default:
			System.out.println("No existeix aquest opcode");
		}
		
	}
	
	/*
	 * override Tostring per tal de veure info del cpu
	 */
	public String toString() {
		return  "RegistreA: " + registres.readA() + "\r\n" +
				"RegistreB: " + registres.readB() + "\r\n" + 
				"RegistreC: " + registres.readC() + "\r\n" +
				"flagZero: " + flags.getZ() + "\r\n" +
				"RegistrePC: " + registres.readPC();
	}
}
