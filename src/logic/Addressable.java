package logic;

/**
 * Interficie que definira totes les classes que son addressables
 * Per exemple la memoria o el I/O
 * @author Aleix Abengochea
 *
 */
public interface Addressable {

	/**
	 * Metode que permete escriure data sobre una direcccio del component
	 * @param address Address on es guardara la data
	 * @param data byte a guardar
	 */
	public void writeData(short address, byte data);

	/**
	 * Metode que servira per llegir dades del registre
	 * @param address Addressa de la qual es vol obtenir la dada
	 * @return
	 */
	public byte readData(short address);
}
