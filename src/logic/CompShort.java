package logic;

public class CompShort {
	private final byte high;
	private final byte low;
	
	public CompShort(byte high, byte low) {
		this.high = high;
		this.low = low;
	}
	
	public byte getHigh() {
		return this.high;
	}
	
	public byte getLow() {
		return this.low;
	}

}
